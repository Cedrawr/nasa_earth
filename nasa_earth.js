var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=2017-08-21";
var myApiKey = "api_key=E9r0MepRkRdiodT88O2OKp40VUClnUPToRIjfg0W";

var example_image_url = "http://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20170821151450.png?api_key=E9r0MepRkRdiodT88O2OKp40VUClnUPToRIjfg0W";
var epic_natural_archive_base = "http://api.nasa.gov/EPIC/archive/natural";
var api_url_query_base = "http://epic.gsfc.nasa.gov/api/natural/date/";
var reqURL = "http://epic.gsfc.nasa.gov/api/natural/2017-08-21?apikey=E9r0MepRkRdiodT88O2OKp40VUClnUPToRIjfg0W"
var imgURL = "http://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20170821151450.png?api_key=E9r0MepRkRdiodT88O2OKp40VUClnUPToRIjfg0W"
// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {

  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);

  // process the "future" img element dynamically generated
  $("div").on("click", "img", function(){
    console.log(this.src);
    // call render_highres() and display the high resolution
  });

  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

    // get the value of the input search text box => date
    var date = '2017-08-21'
    // build an info object to hold the search term and API key
    var info = {};
    var date_array = date.split('-');
    info.year = date_array[0];
    info.month = date_array[1];
    info.day = date_array[2];
    info.api_key = myApiKey[0];

    // build the search url and sling it into the URL request HTML element
    // var search_url = ?
    var search_url = api_url_query_base + info.year + "-" + info.month + "-" + info.day + "-" + info.api_key;

    console.log(search_url);
    // sling it!

    // make the jQuery AJAX call!
    $.ajax({
      url: search_url,
      success: function(data) {
        render_images(data,info);
      },
      cache: false
    });
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data,info) {
    // get NASA earth data from search results data
    // console.log(data.length);
    var images = [];
    for (var i = 0; i < data.length; i++) {
      // build an array of objects that captures all of the key image data
      // => image url
      // => centroid coordinates to be displayed in the caption area
      // => image date to be displayed in the caption area (under thumbnail)
    }


    // select the image grid and clear out the previous render (if any)
    var earth_dom = $('#image-grid');
    earth_dom.empty();

    // render all images in an iterative loop here!
  }

  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
  function render_highres(src_url) {
    // use jQuery to select and maniupate the portion of the DOM => #image-grid
    //  to insert your high resolution image
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});
